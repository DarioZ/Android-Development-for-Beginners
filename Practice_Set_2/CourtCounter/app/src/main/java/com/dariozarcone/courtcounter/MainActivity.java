package com.dariozarcone.courtcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int scoreTeamA = 0;
    int scoreTeamB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("scoreA", scoreTeamA);
        outState.putInt("scoreB", scoreTeamB);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        scoreTeamA = savedInstanceState.getInt("scoreA");
        scoreTeamB = savedInstanceState.getInt("scoreB");
        displayForTeam(scoreTeamA, R.id.team_a_score);
        displayForTeam(scoreTeamB, R.id.team_b_score);
    }
/*
    public void addThreeForTeamA(View view) {
        scoreTeamA += 3;
        displayForTeamA(scoreTeamA);
    }

    public void addTwoForTeamA(View view) {
        scoreTeamA += 2;
        displayForTeamA(scoreTeamA);
    }

    public void addOneForTeamA(View view) {
        scoreTeamA += 1;
        displayForTeamA(scoreTeamA);
    }
 */

    /**
     * Adds points to global variable scoreTeamA based on the tag of the button.
     * Thanks to http://stackoverflow.com/questions/27192219/ for android:tag
     * Thanks to http://stackoverflow.com/questions/5071040/ for java type conversion
     */
    public void addPointsForTeamA(View view) {
        String tag = view.getTag().toString();
        int points = Integer.valueOf(tag);
        scoreTeamA += points;
        displayForTeam(scoreTeamA, R.id.team_a_score);
    }

    /**
     * Adds points to global variable scoreTeamB based on the tag of the button.
     */
    public void addPointsForTeamB(View view) {
        String tag = view.getTag().toString();
        int points = Integer.valueOf(tag);
        scoreTeamB += points;
        displayForTeam(scoreTeamB, R.id.team_b_score);
    }

    /**
     * Resets the score to 0 for both teams. Behavior of reset button.
     */
    public void resetScore(View view) {
        scoreTeamA = 0;
        scoreTeamB = 0;
        displayForTeam(scoreTeamA, R.id.team_a_score);
        displayForTeam(scoreTeamB, R.id.team_b_score);
    }

    /**
     * Displays the given score for any Team, based on the score variable and the view's id.
     *
     * @param score global variable, e.g. scoreTeamA and scoreTeamB
     * @param id of the view, like R.id.id_name
     */
    private void displayForTeam(int score, int id) {
        TextView scoreView = (TextView) findViewById(id);
        scoreView.setText(String.valueOf(score));
    }
}
