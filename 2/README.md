# Just Java
Prima app con codice Java del corso.

Sotto i concetti principali della seconda parte del corso.

## Activity
Un'app è organizzata in **Activity**, ovvero in "schermate" con particolari funzioni. Se le funzioni e i layout sono diversi, anche le activity sono diverse; infatti, ad ogni activity è strettamente legato un file *.java*. L'activity principale di un'app android è **MainActivity**, il cui file .java è `MainActivity.java`. Ad una activity è legato un layout, di default per MainActivity è `activity_main.xml`.
### Button
Un modo per interagire è un pulsante, che in xml è:
```xml
<Button
        android:layout_width="width"
        android:layout_height="height"
        android:onClick="metodo"
        android:text="textOfTheButton"/>
```
in cui `android:onClick:"metodo"` indica il **metodo** di *MainActivity.java* da eseguire al click del pulsante.

## Layout
Per costruire un layout di un'app è possibile inserire *ViewGroup* all'interno di un altro *ViewGroup*. I **nested ViewGroups** sono molto comuni e necessari per semplificare un layout complicato in tanti piccoli layout più semplici.

## Java Notes
### Data types
Come in C, si usa dichiarare una variabile per poterla utilizzare successivamente. Come in C e python, esiste uno *scope* della variabile, che può essere globale o relativa alla funzione specifica (*metodo* di una class). Per dichiarare una variabile si utilizza la sintassi `datatype variableName = value`. In particolare, tra i data type esistono:
-   **byte** a 8 bit, da -128 a 127
-   **short** a 16 bit, da -32768 a 32767
-   **int** a 32 bit
-   **long** a 64 bit
-   **float** a 32 bit e virgola mobile
-   **double** a 64 bit e virgola mobile
-   **boolean** `true` o `false`
-   **char** carattere unicode a 16 bit da `'\u0000'` a `'\uffff'` sempre compreso tra '' come `char ch = 'a';`
-   **String** maiuscolo perché è un oggetto e come tale immutabile se non con metodi specifici, insieme di caratteri compresi tra "" come `String str = "string";`
### String
La dichiarazione di una stringa è:
```java
String str = "stringa";
```
E' possibile concatenare le stringhe con `+` ed è possibile concatenare implicitamente String con numeri usando lo stesso operatore:
```java
str = str + 100;
```
Documentazione sull'oggetto String nella documentazione di java [qui][strings].

## Risorse utili
-   Lista dei [data types][data types] nella documentazione ufficiale di java
-   Lista delle [escape sequences][escape sequences] disponibili nella documentazione ufficiale di java

[escape sequences]: https://docs.oracle.com/javase/tutorial/java/data/characters.html
[data types]: https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html
[strings]: https://docs.oracle.com/javase/tutorial/java/data/strings.html