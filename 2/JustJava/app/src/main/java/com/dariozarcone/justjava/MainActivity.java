package com.dariozarcone.justjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {

    // public static final double COFFEE_PRICE = 0.90;
    int quantity = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        float coffee_price = 0.90f;
        String price = NumberFormat.getCurrencyInstance().format(quantity * coffee_price);
        String message =  "Total: " + price + "\nThank you.";
        displayMessage(message);
        // displayPrice(quantity * coffee_price);
    }

    /**
     * This method increments the numberOfCoffees variable.
     */
    public void increment(View view) {
        quantity += 1;
        display(quantity);
    }

    /**
     * This method decrements the numberOfCoffees variable.
     */
    public void decrement(View view) {
        if (quantity > 0) quantity -= 1;
        display(quantity);
    }

    /**
     * This method displays the given quantity value on the screen
     */
    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }

    /**
     * This method displays the given price on the screen.
     */
    private void displayPrice(double number) {
        TextView priceTextView = (TextView) findViewById(R.id.price_text_view);
        priceTextView.setText(NumberFormat.getCurrencyInstance().format(number));
    }


    /**
     * This method displays the given text on the screen.
     */
    private void displayMessage(String message) {
        TextView priceTextView = (TextView) findViewById(R.id.price_text_view);
        priceTextView.setText(message);
    }

    /**
     * Questo metodo resetta l'ordine
     */
    public void cancelOrder(View view) {
        String message = "Free";
        quantity = 0;
        display(quantity);
        displayMessage(message);
    }
}
