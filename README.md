# Android development for beginners
Studio di Android seguendo il corso "[Android development for beginners][udacity course]" di Udacity e Google.
## Indice
### Parte prima: Views
-   [Generale](./1) sulle **Views**, cosa sono, sintassi di **XML**
-   [TextView](./1/TextView.md) per mostrare testo
-   [ImageView](./1/ImageView.md) per mostrare immagini
-   [ViewGroup](./1/ViewGroup.md): raggruppamenti di Views diverse e posizionamento di queste sullo schermo
-   [Spaziature](./1/Spacing.md): differenze tra padding e layout_margin
-   Prima app, [Birthday Card](./Practice_Set_1/HappyBirthday)

### Parte seconda: Java e app
-   [Generale](./2) sulle **activity**
-   [Button](./2): come interagire con l'utente
-   [Layout](./2) complessi con i **nested ViewGroups**
-   [Data types](./2) di **Java**
-   [Debug][debug] con Android Studio
-   Prima app con codice Java, [Just Java](./2/JustJava)
-   Contatore di punti per il basket, [Court Counter](./Practice_Set_2/CourtCounter)

### Parte terza: classi e oggetti
-   [Metodi, classi e oggetti](./3) e principi di OOP
-   [Tipi di Views](./3) come **oggetti Java** che estendono la classe View
-   [Inheritance e casting](./3)
-   [Resources](./3) dell'app e come accedervi da Java e da XML
-   [Cenni sull'activity cycle](./3)
-   App di test [Cookies](./3/Cookies) e [Cookies alternative](./3/Cookiesalternative)

## Materiali utilizzati
-   Android Studio, IDE ufficiale di Android, per lo sviluppo delle app
-   Sublime Text per Markdown e XML nella prima parte
-   [Visulizzatore XML][xml visualizer] al posto di Android Studio nella prima parte

[udacity course]: https://www.udacity.com/wiki/ud837
[xml visualizer]: http://labs.udacity.com/android-visualizer/#/android/text-view
[debug]: https://developer.android.com/studio/debug/index.html
