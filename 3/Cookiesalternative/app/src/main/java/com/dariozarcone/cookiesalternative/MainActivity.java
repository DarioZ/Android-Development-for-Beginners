package com.dariozarcone.cookiesalternative;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Method called when the eat button is pressed.
     * After the method call the eat button is GONE in favor of the reset button, now VISIBLE.
     */
    public void eatCookie(View view) {
        ImageView image = (ImageView) findViewById(R.id.android_cookie_image_view);
        TextView status = (TextView) findViewById(R.id.status_text_view);
        View reset = findViewById(R.id.button_reset);
        image.setImageResource(R.drawable.after_cookie);
        image.setContentDescription(getString(R.string.after_description));
        status.setText(R.string.after_text);
        view.setVisibility(View.GONE);
        reset.setVisibility(View.VISIBLE);
    }

    /**
     * Method called when the reset button is pressed.
     * Opposite to eatCookie method, after the execution reset button isn't displayed in favor of
     * the eat cookie button.
     */
    public void reset(View view) {
        ImageView image = (ImageView) findViewById(R.id.android_cookie_image_view);
        TextView status = (TextView) findViewById(R.id.status_text_view);
        View eatButton = findViewById(R.id.button_eat);
        image.setImageResource(R.drawable.before_cookie);
        image.setContentDescription(getString(R.string.before_description));
        status.setText(R.string.before_text);
        view.setVisibility(View.GONE);
        eatButton.setVisibility(View.VISIBLE);
    }
}
