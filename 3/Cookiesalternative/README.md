# Cookies alternative
Android mangia il biscotto se viene premuto il tasto e mostra il pulsante reset per tornare alla situazione iniziale.
![layout](./Cookiesalternative_layout.png) | ![layout reset](./Cookiesalternative_reset-layout.png)
------------------------------------------ | -------------------------------------------------------

Applicazione identica a cookies, ma differente nel modo in cui opera.
Non utilizza i tag e i metodi di Button all'interno di MainActivity per modificare il comportamento e l'aspetto dell'unico button, ma definisce a priori in activity_main.xml **due button**: uno di reset e uno per "mangiare il biscotto".
In questo modo, settando la **visibilità** di uno dei due button su *View.GONE* è possibile utilizzarli come due button diversi con azione *onClick* differente, ma mostrarne solamente uno e sempre nella stessa posizione.
L'unica cosa che varia nel MainActivity.java è la visibilità dei button, gestita tramite la funzione `View.setVisibility`:
```java
View eatButton = findViewById(R.id.button_eat);
view.setVisibility(View.GONE);
eatButton.setVisibility(View.VISIBLE);
```
ricordando che view è il button da cui viene chiamata la funzione ed è parametro necessario affinché la funzione venga assegnata ad `android:onClick`.
