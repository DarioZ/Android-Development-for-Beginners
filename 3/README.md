# Lesson 3

## Java
### Metodi
Un **metodo** è una funzione nel linguaggio Java, appartenente ad una classe. La sua dichiarazione nel codice Java è del tipo
```java
access_modifier return_data_type name(input_data_type input_param, ...) {
    code
}
/*
* Metodo che calcola il prezzo del caffé in base alla quantità.
*
* @param quantity of coffees ordered
* @return the price
*/
private float calculatePrice(int quantity) {
    float coffee_price = 0.90f;
    return quantity * coffee_price;
}
```

Il commento serve a creare un **javadoc** e prevede la descrizione del metodo, dei parametri e di quello che ritornano. L'*access modifier* in Java serve a definire se un metodo può essere accessibile dalle altre classi (vedere documentazione di java [qui][access modifier]); il **return data type** dipende dal tipo di dati che si vuole fare ritornare alla funzione, se non si vogliono fare ritornare dati allora si usa `void`, altrimenti uno dei tipi primitivi o anche un oggetto (vedere [qui][return]); anche i parametri possono avere come tipo uno dei primitivi o un oggetto come `String`. E' interessante notare come, usando `void` come *return data type* non sia necessario utilizzare `return` alla fine del metodo, dato che non ritorna niente; ciò nonostante, è possibile usare `return;` per uscire dal metodo anche prima dell'esecuzione delle operazioni successive.

I metodi nella documentazione di Java [qui][methods].


### Classes
Le risorse XML interagiscono con il codice Java così che quando l'activity viene creata (`onCreate`) viene settato il layout (`setContentView(layout)`) e a partire da questo layout viene costruita una *gerarchia* con oggetti le Views del layout. Le Views sono dunque **oggetti**, con **valori e metodi definiti dalla rispettiva classe**, i valori definiti nell'XML e i metodi accessibili con codice Java (e.g. `TextView.setText(String)`). Inoltre ogni particolare View *estende* la classe View (vedere android reference per [TextView][textview], ad esempio, o la rappresentazione semplificata della classe [qui][simplified textview], meglio dell'originale da 10000 e più righe di codice).

Al contrario di metodi e variabili che sono in camelCase con la lettera iniziale minuscola, le classi sono in CamelCase con la lettera iniziale maiuscola. Solitamente per indicare all'interno di una classe una variabile globale che indica lo "stato" dell'oggetto si utilizza `mVariableName`, con *m* che sta per **member variable**.

Le classi hanno la possibilità di **ereditare** da altre classi metodi e stati, e quindi di  *estenderle*.
Questo avviene attraverso l'utilizzo di `extend`:
```java
class TextView extends View {

//CODE

}
```
In questo modo la classe superiore viene chiamata *superclasse* ed i suoi metodi e stati sono accessibili tramite `super.<metodo/stato>` e la classe creata è sottoclasse (*subclass*) di `View`. TextView eredita tutti i metodi e gli stati di View, e ha la possibilità di aggiungere nuovi stati/metodi e di sovrascrivere i metodi/stati della superclasse usando
```
@Override
superClassMethod(...) {
//CODE
}
```
Il metodo originario di View risulta accessibile usando `super.<method/state>`.

Le classi nella documentazione Java [qui][classes].


### Objects
Gli *oggetti* Java sono appunto le "case" costruite in base al piano definito nella rispettiva classe, o le *istanze* della classe. Ogni classe ha un **costruttore** per il rispettivo oggetto, che può essere richiamato con la sintassi:
```java
Object name = new Object(input parameters);
TextView text = new TextView(context);
```
oppure con particolari metodi predefiniti nella classe:
```java
Object name = Object.create-method(input parameters);
Toast toast = Toast.makeText(context, message, duration);
```
solitamente si usa il costruttore, in particolari casi si usa un *factory method*.

Dato che ogni oggetto ha **stati** (variabili) e **metodi**, è possibile accedere a questi usando la notazione `.`:
```java
TextView text = new TextView(context);
text.setText("stringa di testo");
```
se le variabili o i metodi sono dichiarati come **private** sono accessibili solo dai metodi dell'oggetto stesso (e quindi dentro la rispettiva classe); se sono settati come **public** sono accessibili da qualunque altra classe (come `TextView.setText(string)`).

Inoltre è possibile il **casting** degli oggetti: se si ha un oggetto di classe View generico, che sappiamo essere superclasse di TextView, è possibile "rendere" quell'oggetto una TextView usando `(TextView) Object` (vedere su JustJava i metodi display: `findViewById(id)` ritorna una View, per cui è necessario il casting per accedere ai metodi di TextView). Il casting non funziona se effettivamente la View non è una TextView ma, ad esempio un Button.

Vedere anche [qui][inheritance]


## Android
### Resources
Le **resources** si trovano nella cartella **res** e rappresentano i vari tipi di dati che può gestire un'app android. Al suo interno si trovano gli *.xml* di *layout*, valori particolari (*values*), immagini (*drawable*), l'icona o altri tipi di file (*raw*). Ognuno di questi valori è richiamabile in Java usando la classe `R`, creata da `aapt` in automatico. Per ogni tipo di risorsa vi è una sottoclasse di R: la sintassi per accedere agli oggetti in `R` è:
```java
R.resource_type.object
R.layout.activity_main
R.string.hello
R.drawable.icon
R.color.red
R.id.text_view
```
per accedere alle risorse in XML si usa invece la sintassi:
```xml
@resource_type/resource
@layout/activity_main
@string/hello
@drawable/icon
@color/red
@id/text_view
```
Dettagli sulle risorse nella documentazione di android [qui][resources].

### strings.xml e localizzazione
Per permettere una semplice localizzazione è necessario definire tutte le stringhe di testo dell'app nell'apposito `strings.xml`. Le string vengono definite con il tag XML:
```xml
<resources>
    <string name="app_name" translatable="false">Just Java</string>
    <string name="name">Name</string>
</resources>
```
e vengono richiamate in XML tramite `@string/name` e in Java tramite `R.string.name` per avere l'id o `getString(R.string.name)` per avere un oggetto di tipo String. Se la stringa contiene parametri è conveniente usare xliff:
```xml
<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">
    <string name="order_summary_name">Name: <xliff:g id="name" example="Dario">%s</xliff:g></string>
</resources>
```
in cui è possibile assegnare un valore tramite `getString(R.string.order_summary_name, Amy)` ad esempio, in modo simile al metodo .format di String.

In Android Studio è presente un editor grafico delle traduzioni per la localizzazione. Per altri consigli su localizzazione vedere nella documentazione di Android [qui][localization]

### styles.xml
All'interno di styles.xml è possibile centralizzare gli attributi XML che gestiscono la visualizzazione delle Views (altezza, larghezza, colori, padding...) utilizzando gli **stili**. Ogni stile è un insieme di attributi da assegnare a varie Views che saranno, logicamente, simili. Di default è predefinito il tema principale, AppName, che estende Theme.AppCompat.Light.DarkActionBar e che nel manifest viene applicato a tutta la MainActivity; è possibile però aggiungere altri stili relativi ad un solo tipo di View, come
```xml
<resources>
    <!-- Header style according to https://material.google.com/components/lists.html#lists-specs-->
    <style name="HeaderTextStyle">
        <item name="android:layout_width">wrap_content</item>
        <item name="android:layout_height">48dp</item>
        <item name="android:gravity">center_vertical</item>
        <item name="android:textSize">15sp</item>
        <item name="android:textAllCaps">true</item>
    </style>
</resources>
```
lo stile viene poi associato alla View usando l'attributo `style="HeaderTextStyle"`.

### Metodi particolari
MainActivity.java di default estende la Activity predefinita, **AppCompatActivity**. Il primo metodo che viene eseguito dall'activity al momento della sua creazione è `onCreate`, che viene sovrascritto per potere, oltre a `super.onCreate`, mostrare sullo schermo il layout definito (ma potrebbe anche essere una View) usando `setContentView`.

Eredita da AppCompatActivity altri metodi relativi all'*activity cycle* (vedere sotto) come onPause, onSaveInstanceState etc., il metodo `findViewById(id)` che ritorna un oggetto di tipo View che è proprio la View con id specificato.

Allo stesso modo, per interagire con le View basta assegnare ad un oggetto di tipo indicato la View tramite findViewById(id), senza dimenticare il casting necessario ad utilizzare i metodi del tipo di View specifico:
```java
TextView text = (TextView) findViewById(R.id.text_view);
```
e dunque usare i metodi del tipo di View specifico:
```java
text.setText(string);       //setta testo della TextView
text.setTextSize(int);      //font size in sp
text.setVisibility(int);    //attributo di View, e non solo di TextView, può essere
                            //View.VISIBLE, View.INVISIBLE, View.GONE
String textOfTextView = text.getText().toString();
```
per ogni attributo xml c'è un metodo specifico: vedere [documentazione di TextView][textview] ad esempio.

### Altre views
EditText estende TextView mostrando uno spazio modificabile su cui scrivere. `inputType` modifica il comportamento della tastiera e il suo layout (numeri, autocompletamento, password...), `hint` è il testo mostrato quando non viene scritto nulla.
```xml
<EditText
            android:id="@+id/name_field"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginBottom="16dp"
            android:hint="@string/name"
            android:inputType="textCapWords"/>
```

CheckBox estende Button mostrando un check box con del testo accanto. La posizione del testo si può gestire con padding (per aumentare lo spazio effettivo tra il testo e il check box) o con gravity.
```xml
<CheckBox
            android:id="@+id/check_whipped_cream"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:paddingLeft="24dp"
            android:text="@string/whipped_cream"
            android:textSize="16sp"/>
```
il metodo per controllare se la check box è spuntata o no è `CheckBox.isChecked()` che ritorna un valore `boolean` true o false:
```java
CheckBox check = (CheckBox) findViewById(R.id.check_box);
boolean checked = check.isChecked();
```

### Intents
Gli **intent** sono un modo per inviare informazioni ad altre app: è possibile settare sveglie nell'app predefinita, aggingere eventi, inviare email, avviare la fotocamera, aprire google maps ed altro. E' importante notare che, a prescindere dall'app che l'utente usa, automaticamente si apre un popup di scelta per decidere a quale app assegnare l'azione. Ad esempio, se l'intent si riferisce ad aprire un sito internet, sarà possibile scegliere quale web browser usare. Il codice è:
```java
Intent intent = new Intent(Intent.ACTION_SENDTO);   //ACTION_SENDTO è una delle varie costanti definite in
                                                    //Intent, ognuna che si riferisce a un tipo diverso di
                                                    //Intent
intent.setData(Uri.parse("mailto:email_address"));  //setData serve a settare i dati necessari senza la quale
                                                    //l'intent non può esistere. L'input è un Uri, diverso
                                                    //per ogni tipo di intent, e necessita di Uri.parse(string)
intent.putExtra(Intent.EXTRA_SUBJECT,
                    email_subject_string);          //putExtra setta informazioni aggiuntive all'intent
                                                    //il primo input è una costante che indica il tipo di extra
                                                    //il secondo input è il valore assegnato all'extra
intent.putExtra(Intent.EXTRA_TEXT, message);

if(intent.resolveActivity(getPackageManager()) != null) {
    startActivity(intent);
}                                                   //necessario affinché non vi siano crash: avvia l'intent
                                                    //solo se esiste un'app in grado di gestirlo
```


### On your own: activity cycle
Ogni activity viene distrutta e ricreata in un determinato modo ogni volta che c'è un qualche tipo di cambiamento (e.g. orientation). Per evitare che in questo processo si perdano dati viene automaticamente avviata da android, alla chiusura dell'activity, il metodo `onSaveInstanceState(Bundle outState)` che di default dovrebbe salvare i dati delle Views, per poi riprenderli appena l'attività viene riavviata con `onRestoreInstanceState(Bundle savedInstanceState)`. Per salvare anche i valori di variabili particolari etc. (vedere MainActivity.java in CourtCounter) è necessario sovrascrivere i metodi affinché vengano salvate le variabili in questione all'interno dell'apposito oggetto Bundle, che viene resettato solo alla chiusura completa dell'app.
```java
@Override
protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);    //avvia l'originale onSaveInstanceState
    outState.putInt("scoreA", scoreTeamA);  //assegna il nome "scoreA" alla int globale scoreTeamA e la pone all'interno del Bundle
    }

@Override
protected void onRestoreInstanceState(Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);   //avvia l'originale onRestoreInstanceState
    scoreTeamA = savedInstanceState.getInt("scoreA");   //riprende il valore int salvato in precedenza nel Bundle con il nome "scoreA" e lo assegna alla variabile globale scoreTeamA
    }
```
Vedere il ciclo delle activity [qui][activity cycle].

Vedere la documentazione su `onSaveInstanceState(Bundle outState)` [qui][onsaveinstancestate].

Vedere la documentazione sull'oggetto Bundle [qui][bundle].




[classes]: https://docs.oracle.com/javase/tutorial/java/concepts/class.html
[resources]: https://developer.android.com/guide/topics/resources/overview.html
[return]: https://docs.oracle.com/javase/tutorial/java/javaOO/returnvalue.html
[arguments]: https://docs.oracle.com/javase/tutorial/java/javaOO/arguments.html
[access modifier]: https://docs.oracle.com/javase/tutorial/java/javaOO/accesscontrol.html
[textview]: https://developer.android.com/reference/android/widget/TextView.html
[simplified textview]: https://gist.github.com/udacityandroid/47592c621d32450d7dbc
[methods]: https://docs.oracle.com/javase/tutorial/java/javaOO/methods.html
[activity cycle]: https://developer.android.com/guide/components/activities.html#Lifecycle
[onsaveinstancestate]: https://developer.android.com/reference/android/app/Activity.html#onSaveInstanceState%28android.os.Bundle%29
[bundle]: https://developer.android.com/reference/android/os/Bundle.html
[inheritance]: https://docs.oracle.com/javase/tutorial/java/IandI/subclasses.html
[localization]: https://developer.android.com/distribute/tools/localization-checklist.html
