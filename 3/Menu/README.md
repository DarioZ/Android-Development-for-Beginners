# Menu - test di log
Mostra tre TextView, alla pressione del pulsante ne prende il valore e lo invia come log.

Attraverso `Log` si possono inviare messaggi di diversa importanza utili a capire come funziona l'applicazione.
I messaggi vengono letti una volta che il device è collegato al pc, attraverso "Android monitor" su Android Studio.

Il messaggio viene mostrato nel monitor nella forma:
```
LEVEL/tag: message
W/KeyCharacterMap: No keyboard for id 0
```
In cui LEVEL può essere V, D, I, W, E, WTF, corrispondente alle diverse categorie di importanza (vedere sotto).

Per la diversa importanza dei messaggi ci sono diverse funzioni:
```java
Log.v(tag, message);    //verbose
Log.d(tag, message);    //debug
Log.i(tag, message);    //information
Log.w(tag, message);    //warning
Log.e(tag, message);    //error
Log.wtf(tag, message);  //..."what a terrible failure!"
```
Nell'android monitor si può scegliere il "livello" di messaggi da mostrare: da verbose (tutti), da debug, etc.

Ulteriori dettagli nella documentazione di android [qui][log].

[log]: https://developer.android.com/studio/command-line/logcat.html
