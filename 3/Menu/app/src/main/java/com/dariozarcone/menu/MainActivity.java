package com.dariozarcone.menu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void printToLogs(View view) {
        // Find first menu item TextView and print the text to the logs
        TextView firstItem = (TextView) findViewById(R.id.menu_item_1);
        Log.i("Menu item 1", firstItem.getText().toString());
        // Find second menu item TextView and print the text to the logs
        TextView secondItem = (TextView) findViewById(R.id.menu_item_2);
        Log.i("Menu item 2", secondItem.getText().toString());
        // Find third menu item TextView and print the text to the logs
        TextView thirdItem = (TextView) findViewById(R.id.menu_item_3);
        Log.i("Menu item 3", thirdItem.getText().toString());
    }
}
