package com.dariozarcone.justjava;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {

    // public static final double COFFEE_PRICE = 0.90;
    int quantity = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("quantity", quantity);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        quantity = savedInstanceState.getInt("quantity");
        displayQuantity(quantity);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        CheckBox whippedCreamCheckBox = (CheckBox) findViewById(R.id.check_whipped_cream);
        CheckBox chocolateCheckBox = (CheckBox) findViewById(R.id.check_chocolate);
        EditText nameField = (EditText) findViewById(R.id.name_field);
        boolean hasWhippedCream = whippedCreamCheckBox.isChecked();
        boolean hasChocolate = chocolateCheckBox.isChecked();
        float price = calculatePrice(hasWhippedCream, hasChocolate);
        String name = nameField.getText().toString();
        String subject = getString(R.string.subject, name);
        String message = createOrderSummary(name , price, hasWhippedCream, hasChocolate);
        sendOrder(message, subject);
    }

    /**
     * Create a summary of the order.
     *
     * @param name            of the customer
     * @param price           of the order
     * @param hasWhippedCream or not
     * @param hasChocolate    or not
     * @return summary of the order
     */
    private String createOrderSummary(String name, float price, boolean hasWhippedCream, boolean hasChocolate) {
        String message;
        String cream = boolToString(hasWhippedCream);
        String chocolate = boolToString(hasChocolate);
        String priceString = NumberFormat.getCurrencyInstance().format(price);
        message = getString(R.string.order_summary_name, name);
        message += getString(R.string.order_summary_cream, cream);
        message += getString(R.string.order_summary_chocolate, chocolate);
        message += getString(R.string.order_summary_quantity, quantity);
        message += getString(R.string.order_summary_total, priceString);
        message += getString(R.string.order_summary_thank);
        return message;
    }

    /**
     * Calculate the price of the order.
     */
    private float calculatePrice(boolean hasWhippedCream, boolean hasChocolate) {
        float coffeePrice = 1.50f;
        float creamPrice = 0.5f;
        float chocolatePrice = 1.0f;
        if (hasWhippedCream) coffeePrice += creamPrice;
        if (hasChocolate) coffeePrice += chocolatePrice;
        return quantity * coffeePrice;
    }

    /**
     * This method increments the numberOfCoffees variable.
     */
    public void increment(View view) {
        if(quantity < 100) quantity += 1;
        else Toast.makeText(this, R.string.error_much, Toast.LENGTH_SHORT).show();
        displayQuantity(quantity);
    }

    /**
     * This method decrements the numberOfCoffees variable.
     */
    public void decrement(View view) {
        if(quantity > 1) quantity -= 1;
        else Toast.makeText(this, R.string.error_few, Toast.LENGTH_SHORT).show();
        displayQuantity(quantity);
    }

    /**
     * This method displays the given quantity value on the screen
     */
    private void displayQuantity(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }

    /**
     * This method displays the given text on the screen.
     */
    private void sendOrder(String message, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        if(intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
        else Toast.makeText(this, R.string.error_no_email, Toast.LENGTH_SHORT).show();
    }

    private String boolToString(boolean value){
        String string_message;
        if(value) string_message = getString(R.string.yes);
        else string_message = getString(R.string.no);
        return string_message;
    }

}
