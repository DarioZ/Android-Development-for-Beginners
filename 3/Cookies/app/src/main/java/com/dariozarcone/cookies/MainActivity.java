package com.dariozarcone.cookies;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.button);
        button.setTag("eat");
    }

    /**
     * Method called when the button is pressed.
     * Uses button tags.
     */
    public void buttonListener(View view) {
        String tag = view.getTag().toString();
        if(tag.equals("eat")) {
            eatCookie();
        }
        else if(tag.equals("reset")) {
            reset();
        }
    }

    /**
     * Called when the cookie should be eaten.
     */
    private void eatCookie() {
        ImageView image = (ImageView) findViewById(R.id.android_cookie_image_view);
        TextView status = (TextView) findViewById(R.id.status_text_view);
        Button button = (Button) findViewById(R.id.button);
        image.setImageResource(R.drawable.after_cookie);
        image.setContentDescription(getString(R.string.after_description));
        status.setText(R.string.after_text);
        button.setTag("reset");
        button.setText(R.string.button_reset);
    }

    /**
     * Called to reset after the cookie has been eaten
     */
    private void reset() {
        ImageView image = (ImageView) findViewById(R.id.android_cookie_image_view);
        TextView status = (TextView) findViewById(R.id.status_text_view);
        Button button = (Button) findViewById(R.id.button);
        image.setImageResource(R.drawable.before_cookie);
        image.setContentDescription(getString(R.string.before_description));
        status.setText(R.string.before_text);
        button.setTag("eat");
        button.setText(R.string.button_text);
    }
}
