# Cookies
Android mangia il biscotto se viene premuto il tasto e mostra il pulsante reset per tornare alla situazione iniziale.
![layout](./Cookies_layout.png) | ![layout reset](./Cookies_reset-layout.png)
------------------------------- | --------------------------------------------

Alla pressione del button modifica ImageView, TextView e Button stesso, gestendo tutto all'interno del codice Java.
Usa i **tag** del button, opportunamente settati tramite `View.setTag(Object)` per capire lo stato del button (reset o eat) e agire di conseguenza.
Il metodo che viene avviato con `android:onClick` è uno solo, `buttonListener(View view)` che secondo il valore di `view.getTag()` esegue eatCookie() o reset().
