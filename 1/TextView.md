# TextView
View che serve a visualizzare del testo.

Utilizzare il [visualizzatore di XML][xml visualizer] per test.

## Attributi di `TextView`
Una lista di vari **attributi** dell'elemento XML `TextView`. La lista di tutti gli attributi di una TextView nella documentazione android è [qui][textview attributes]

N.B. XML è **case-sensitive**

-   `android:text="string"` testo che la `TextView` visualizza

-   `android:background="color"` colore di **background**, in formato *esadecimale* (completo di `#`) o tra i colori definiti in `@android:color` come `@android:color/darker_gray` o `@android:color/black`

-   `android:textColor="color"` come sopra, ma cambia il **colore del testo**

-   `android:textStyle="bold or italic"` può assumere il valore `"bold"` o `"italic"` o `"normal"` e rende il testo in grassetto in corsivo. E' possibile rendere il testo *bolditalic* usando entrambi i valori legati da `|` (come in `"bold|italic"`

-   `android:textAllCaps="boolean value"` rende il testo tutto maiuscolo se il valore è `"true"`

-   `android:layout_width="dp"` e `android:layout_height="dp"` indicano le **misure** della View. La grandezza è espressa in **dp** [Density independent pixels][dp pixels] per garantire compatibilità con schermi di varie grandezze. Può anche assumere il valore di `"wrap_content"` che si adatta all'altezza e alla larghezza del contenuto all'interno.

-   `android:textSize="size"` indica la grandezza dei caratteri, indicati in **sp** Scale Independent Pixels. Meglio seguire le [*linee guida* del Material Design][typography] o usare i valori speciali, già settati, con `android:textAppearance="valore"`. I valori disponibili sono:

    `"?android:textAppearanceSmall"`

    `"?android:textAppearanceMedium"`

    `"?android:textAppearanceLarge"`

    Notare che impostano anche il colore del testo ad un colore standard. Vedere [qui][textappearance]

[xml visualizer]: http://labs.udacity.com/android-visualizer/#/android/text-view
[dp pixels]: http://www.google.com/design/spec/layout/metrics-keylines.html#metrics-keylines-touch-target-size
[textappearance]: https://plus.google.com/+AndroidDevelopers/posts/gQuBtnuk6iG
[typography]: http://www.google.com/design/spec/style/typography.html
[palette]: http://www.google.com/design/spec/style/color.html#color-color-palette
[common views]: https://drive.google.com/file/d/0B5XIkMkayHgRMVljUVIyZzNmQUU/view?usp=sharing
[textview attributes]: https://developer.android.com/reference/android/widget/TextView.html
