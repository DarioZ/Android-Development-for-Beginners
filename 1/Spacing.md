# Spaziature
Ci sono due principali modi per gestire gli spazi nei layout android, usando due attributi di views (e viewgroups), **padding** e **margin**. Il *padding* è gestito dalla View e il *margin* dal Viewgroup; il padding aumenta lo spazio della View, il margin circonda la View di spazio vuoto
## Padding
`android:padding="dp"` aggiunge il numero prestabilito di dp a destra, sinistra, sopra e sotto la View. Per aggiungere padding solo da un lato usare i seguenti:
-   `android:paddingTop="dp"`
-   `android:paddingBottom="dp"`
-   `android:paddingRight="dp"`
-   `android:paddingLeft="dp"`
## Margin
`android:layout_margin="dp"` che mette il margin di dp a destra, sinistra, sopra e sotto oppure, per aggiungere margin solo da un lato:
-   `android:layout_marginTop="dp"`
-   `android:layout_marginBottom="dp"`
-   `android:layout_marginLeft="dp"`
-   `android:layout_marginRight="dp"`
Si può notare che è gestito dal Viewgroup dal fatto che appartiene a *layout*.
## Guidelines
Seguendo il material design, padding e margin devono avere valori multipli di **8dp**. Vedere dettagli nelle [linee guida][material metrics]


[material metrics]: https://www.google.com/design/spec/layout/metrics-keylines.html
