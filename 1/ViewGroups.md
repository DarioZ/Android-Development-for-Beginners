# ViewGroups
I ViewGroups sono contenitori di views, che servono a mettere insieme views di diversi tipi in un solo layout. La ViewGroup si dice genitore (**parent**) delle views che contiene, che sono figli (**child**). A loro volta, le views appartenenti ad un ViewGroup sono **siblings** tra loro.

La documentazione è qui: [Android ViewGroup](viewgroup)

Un valore importante nei layout è `"match_parent"` da mettere come valore degli attributi che regolano larghezza/altezza. Questo setta width/height a width/height del ViewGroup genitore. Se applicato al ViewGroup principale, setta il layout alla larghezza/altezza dello schermo. (vedere [qui][match_parent])

Esistono vari tipi di ViewGroups, tra cui:
## LinearLayout
`LinearLayout` è un particolare tipo di ViewGroup che ordina le proprie views in una colonna **verticale** o in una riga **orizzontale**.
Per test andare sul visualizzatore di XML [qui][XMLV linear layout]

La lista degli attributi si trova nella documentazione di Android: [Android Linear Layout][linear layout]

Gli attributi tipici sono, oltre ai soliti `android:layout_width` e `android:layout_height`:
-   `android:orientation` che può essere `"vertical"` o `"horizontal"`(valore di default) e indica il modo di ordinare le views che contiene
-   `android:layout_weight="number"` attributo da assegnare alle views children, vedere [la guida][linear layout guide]. In pratica una unità di misura proporzionale alle altre views in altezza se android:orientation="vertical" o in larghezza altrimenti. Maggiore è il "peso", maggiore è lo spazio indicato da weight (se due elementi hanno peso "1" ed uno ha peso "2", lo schermo disponibile verrà "diviso" in 4 parti, due conterranno una view l'uno e altre due conterranno una view in totale). Lo spazio disponibile viene diviso per il peso totale di tutte le views e moltiplicato per il peso di ogni view (nel caso precedente, 1+1+2=4, alle prime due verrà dato 1/4 di spazio e alla terza 2/4).

## Relative Layout
`RelativeLayout` è un particolare tipo di ViewGroup che permette di allineare i children relativamente al ViewGroup (in alto, in basso, a destra, a sinistra) e relativamente ai children nel ViewGroup (sopra una View, accanto una View etc.)

Per potere settare l'allineamento relativo ad un'altra view risulta necessario riconoscere **univocamente** la view usando l'attributo
```xml
android:id="@+id/id_name"
```
in cui `@` si riferisce ad una risorsa dell'apk, `+` comunica che viene aggiunto l'id (viene dichiarato per la prima volta) e `id` è il tipo di risorsa (*resource type*). `id_name` deve cominciare con una lettera, non può avere spazi o segni di punteggiatura.

Lista degli attributi nella documentazione di android: [RelativeLayout][relativelayout doc]

### Attributi di `RelativeLayout`
Allineamento rispetto al parent:
-   `android:layout_alignParentTop`
-   `android:layout_alignParentBottom`
-   `android:layout_alignParentRight`
-   `android:layout_alignParentLeft`
    Si assegnano alle views children e possono essere "true" o "false". Allineano in alto, basso, destra o sinistra del ViewGroup parent.
-   `android:layout_centerHorizontal`
-   `android:layout_centerVertical`
-   `android:layout_centerInParent`
    Si assegnano alle views children e possono essere "true" o "false". Allineano al centro orizzontalmente, verticalmente o nel centro del parent.

Alineamento rispetto ad un'altra view (che fungerà da "ancora", *anchor*):
-   `android:layout_toLeftOf="@id/id_name"`
-   `android:layout_toRightOf="@id/id_name"`
-   `android:layout_above="@id/id_name"`
-   `android:layout_below="@id/id_name"`
Usare [questo test][test relative XMLV] per vedere allineamento

[viewgroup]: https://developer.android.com/reference/android/view/ViewGroup.html
[linear layout]: https://developer.android.com/reference/android/widget/LinearLayout.html
[XMLV linear layout]: http://labs.udacity.com/android-visualizer/#/android/linear-layout
[match_parent]: http://labs.udacity.com/android-visualizer/#/android/match-parent
[linear layout guide]: https://developer.android.com/guide/topics/ui/layout/linear.html
[relativelayout doc]: https://developer.android.com/reference/android/widget/RelativeLayout.LayoutParams.html
[test relative XMLV]: http://labs.udacity.com/android-visualizer/#/android/relative-layout-view-ids
