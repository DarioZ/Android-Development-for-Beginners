# Views
## Cosa sono le Views?
Le **Views** sono "*rettangoli*" che mostrano del contenuto sullo schermo. L'insieme delle Views è quello che si chiama **layout**.

Tra i diversi tipi di Views ci sono:
- `TextView` che servono a mostrare del testo
- `ImageView` che serve a mostrare una immagine
- `Button` che rappresenta un pulsante sullo schermo

## XML
Per disegnare il layout in Android si utilizza la sintassi di **XML**, un *linguaggio di markup* simile ad HTML.
Solitamente si utilizzano dei *self-closing tag* che si aprono con il nome dell'elemento XML (ad esempio `TextView`) e che contengono gli attributi dell'elemento e i rispettivi valori (elemento=valore).
```xml
<TextView
    android:text="Happy Birthday!"
    android:background="@android:color/darker_gray"
    android:layout_width="150dp"
    android:layout_height="75dp" />
```

Per indicare un elemento genitore (*parent*) che ha degli elementi figli (*child*) si utilizza questa altra sintassi:
```xml
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:orientation="vertical">

    ...elementi figli...

</LinearLayout>
```
Ovvero il tag si chiude solo alla fine con `</LinearLayout>` (si utilizza sempre `/` alla chiusura).

`@` si riferisce sempre ad una risorsa (*resource*) dell'apk.

In XML, e in particolare in apertura di ogni file XML per android, è necessario usare i **namespace**. Vedere perché su [HTML.it][namespace]. L'attributo da usare all'inizio del tag del layout XML è:
```xml
xmlns:android="http://schemas.android.com/apk/res/android"
```
E' possibile usare [XML Visualizer][xml visualizer] per lavorare su codice XML senza **Android Studio**.

Vedere [qui][common views] per una lista delle views più comuni

------------------
## Links utili
-   [Visulizzatore XML][xml visualizer]
-   [Documentazione su Density Independent Pixels][dp pixels]
-   [Utilizzo di `TextAppearance`][textappearance]
-   [Lista con esempi di Views comuni][common views]
-   [Documentazione sulle `TextView`][textview attributes]

### Material design:
-   [Linee guida colori][palette]
-   [Linee guida caratteri][typography]
-   [Google I/O app][google i/o app]

[xml visualizer]: http://labs.udacity.com/android-visualizer/#/android/text-view
[dp pixels]: http://www.google.com/design/spec/layout/metrics-keylines.html#metrics-keylines-touch-target-size
[textappearance]: https://plus.google.com/+AndroidDevelopers/posts/gQuBtnuk6iG
[typography]: http://www.google.com/design/spec/style/typography.html
[palette]: http://www.google.com/design/spec/style/color.html#color-color-palette
[common views]: https://drive.google.com/file/d/0B5XIkMkayHgRMVljUVIyZzNmQUU/view?usp=sharing
[textview attributes]: https://developer.android.com/reference/android/widget/TextView.html
[namespace]: http://www.html.it/articoli/il-misterioso-mondo-dei-namespaces-1/
[google i/o app]: http://android-developers.blogspot.it/2014/08/material-design-in-2014-google-io-app.html
