# ImageView
View che visualizza un'immagine.

Utilizzare [la ImageView nel visualizzatore di XML][xml visualizer imageview] per test.

La lista di tutti gli attributi di una ImageView nella documentazione android è [qui][imageview attributes]

## Attributi di `ImageView`

-   `android:src="@file"` che indica l'**immagine da visualizzare**. `@file` è un percorso relativo alla root dell'apk e non include l'estensione *.jpeg* o *.png* etc.
    Dato che in una apk le immagini si chiamano **drawables** e stanno nella cartella `drawable` nella root dell'apk, la sintassi diventa `@drawable/nome-immagine`

-   `android:layout_width` e `android:layout_height`: vedere [TextView.md](TextView.md)

-   `android:scaleType="valore"` scala l'immagine alla dimensione della View. Notare che il valore `"center` non modifica le dimensioni dell'immagine rispetto allo schermo, ma centra l'immagine. `"centerCrop` invece oltre a centrare l'immagine la scala alle dimensioni della View, croppando il resto e mantenendo l'*aspect-ratio*.



N.B. un'immagine che va da un lato all'altro dello schermo senza bordi si chiama *full-bleed image* e si usa molto nel Material Design

[xml visualizer imageview]: http://labs.udacity.com/android-visualizer/#/android/simple-imageview
[imageview attributes]: https://developer.android.com/reference/android/widget/ImageView.html
